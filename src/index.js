import React from "react";
import ReactDOM from "react-dom";
import "react-bootstrap";
import { Login } from "./components/login";
import { Signup } from "./components/signup";
import { Hoome } from "./components/hoome";
import { Tasksfun } from "./components/tasksfun";
import { Userfun } from "./components/user";
import { BrowserRouter, Route, Switch } from "react-router-dom";

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Login} />
        <Route exact path="/signup" component={Signup} />
        <Route exact path="/hoome" component={Hoome} />
        <Route exact path="/tasksfun" component={Tasksfun} />
        <Route exact path="/user" component={Userfun} />
      </Switch>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
