import React from "react";
import { Navig } from "./hoomepage/navig";
import { Dropdown } from "./hoomepage/dropdown";
import { Information } from "./hoomepage/information";

export const Hoome = () => {
  return (
    <>
      <Navig />
      <Dropdown />
      <Information />
    </>
  );
};
