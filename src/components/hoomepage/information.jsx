import React from "react";
import { Container } from "react-bootstrap";
export const Information = () => {
  return (
    <>
      <Container>
        <div className="mt-5" id="webtech" default>
          <blockquote class="blockquote">
            <h1>What is Web Technology?</h1>
            <p>
              You have probably heard the term “web development technologies”
              before, but did you ever think about what it actually means? Since
              computers can’t communicate with each other the way people do,
              they require codes instead. Web technologies are the markup
              languages and multimedia packages computers use to communicate.
              Web development comes with a huge set of rules and techniques
              every website developer should know about. If you want a website
              to look and function as you wish them to, you need to get familiar
              with web technologies that will help you achieve your goal.
              Developing an app or a website typically comes down to knowing 3
              main languages: JavaScript, CSS, and HTML. And while it sounds
              quite complicated, once you know what you are doing, understanding
              web technology and the way it works becomes significantly easier.
              We present you with an introduction to web technologies and the
              latest web technologies list hoping it will make things at least a
              bit easier for you. Now, let’s take a look.
            </p>
            <footer class="blockquote-footer">From Sources</footer>
          </blockquote>
        </div>

        <div className="mt-5" id="bootstrapp" style={{ display: "none" }}>
          <blockquote class="blockquote">
            <h1>What is Bootstrap?</h1>
            <p>
              Bootstrap is a free and open-source CSS framework directed at
              responsive, mobile-first front-end web development. It contains
              CSS- and (optionally) JavaScript-based design templates for
              typography, forms, buttons, navigation, and other interface
              components. Bootstrap is among the most starred projects on
              GitHub, with more than 142,000 stars, behind freeCodeCamp (almost
              312,000 stars) and marginally behind Vue.js framework. Bootstrap,
              originally named Twitter Blueprint, was developed by Mark Otto and
              Jacob Thornton at Twitter as a framework to encourage consistency
              across internal tools. Before Bootstrap, various libraries were
              used for interface development, which led to inconsistencies and a
              high maintenance burden. According to Twitter developer Mark Otto:
              A super small group of developers and I got together to design and
              build a new internal tool and saw an opportunity to do something
              more. Through that process, we saw ourselves build something much
              more substantial than another internal tool. Months later, we
              ended up with an early version of Bootstrap as a way to document
              and share common design patterns and assets within the company.
              After a few months of development by a small group, many
              developers at Twitter began to contribute to the project as a part
              of Hack Week, a hackathon-style week for the Twitter development
              team.
            </p>
            <footer class="blockquote-footer">From Wikipedia</footer>
          </blockquote>
        </div>

        <div className="mt-5" id="jquery" style={{ display: "none" }}>
          <blockquote class="blockquote">
            <h1>What is jQuery?</h1>
            <p>
              jQuery is a JavaScript library designed to simplify HTML DOM tree
              traversal and manipulation, as well as event handling, CSS
              animation, and Ajax. It is free, open-source software using the
              permissive MIT License. As of May 2019, jQuery is used by 73% of
              the 10 million most popular websites. Web analysis indicates that
              it is the most widely deployed JavaScript library by a large
              margin, having at least 3 to 4 times more usage than any other
              JavaScript library. jQuery's syntax is designed to make it easier
              to navigate a document, select DOM elements, create animations,
              handle events, and develop Ajax applications. jQuery also provides
              capabilities for developers to create plug-ins on top of the
              JavaScript library. This enables developers to create abstractions
              for low-level interaction and animation, advanced effects and
              high-level, themeable widgets. The modular approach to the jQuery
              library allows the creation of powerful dynamic web pages and Web
              applications. The set of jQuery core features—DOM element
              selections, traversal and manipulation—enabled by its selector
              engine (named "Sizzle" from v1.3), created a new "programming
              style", fusing algorithms and DOM data structures.
            </p>
            <footer class="blockquote-footer">From Wikipedia</footer>
          </blockquote>
        </div>

        <div className="mt-5" id="ihtml" style={{ display: "none" }}>
          <blockquote class="blockquote">
            <h1>What is HTML?</h1>
            <p>
              Hypertext Markup Language (HTML) is the standard markup language
              for documents designed to be displayed in a web browser. It can be
              assisted by technologies such as Cascading Style Sheets (CSS) and
              scripting languages such as JavaScript. Web browsers receive HTML
              documents from a web server or from local storage and render the
              documents into multimedia web pages. HTML describes the structure
              of a web page semantically and originally included cues for the
              appearance of the document. HTML elements are the building blocks
              of HTML pages. With HTML constructs, images and other objects such
              as interactive forms may be embedded into the rendered page. HTML
              provides a means to create structured documents by denoting
              structural semantics for text such as headings, paragraphs, lists,
              links, quotes and other items. HTML elements are delineated by
              tags, written using angle brackets.
            </p>
            <footer class="blockquote-footer">From Wikipedia</footer>
          </blockquote>
        </div>

        <div className="mt-5" id="icss" style={{ display: "none" }}>
          <blockquote class="blockquote">
            <h1>What is CSS?</h1>
            <p>
              Cascading Style Sheets (CSS) is a style sheet language used for
              describing the presentation of a document written in a markup
              language such as HTML. CSS is a cornerstone technology of the
              World Wide Web, alongside HTML and JavaScript. CSS is designed to
              enable the separation of presentation and content, including
              layout, colors, and fonts. This separation can improve content
              accessibility, provide more flexibility and control in the
              specification of presentation characteristics, enable multiple web
              pages to share formatting by specifying the relevant CSS in a
              separate .css file which reduces complexity and repetition in the
              structural content as well as enabling the .css file to be cached
              to improve the page load speed between the pages that share the
              file and its formatting. Separation of formatting and content also
              makes it feasible to present the same markup page in different
              styles for different rendering methods, such as on-screen, in
              print, by voice (via speech-based browser or screen reader), and
              on Braille-based tactile devices. CSS also has rules for alternate
              formatting if the content is accessed on a mobile device.
            </p>
            <footer class="blockquote-footer">From Wikipedia</footer>
          </blockquote>
        </div>
      </Container>
    </>
  );
};
