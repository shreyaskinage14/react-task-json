import React from "react";
import Boots from "./js/boots";
import jQue from "./js/jq";
import iHTML from "./js/ihtml";
import iCSS from "./js/icss.js";
export const Dropdown = () => {
  return (
    <>
      <div class="dropdown">
        <button
          class="btn-lg mt-4 ml-5 btn-outline-dark dropdown-toggle "
          type="button"
          id="btnDropdownDemo"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          Web Technologies
        </button>
        <div class="dropdown-menu" aria-labelledby="btnDropdownDemo">
          <button class="dropdown-item" id="boot" onClick={Boots}>
            Bootstrap 4
          </button>
          <button class="dropdown-item" id="jq" onClick={jQue}>
            jQuery
          </button>
          <button class="dropdown-item" id="html" onClick={iHTML}>
            HTML
          </button>
          <button class="dropdown-item" id="css" onClick={iCSS}>
            CSS
          </button>
        </div>
      </div>
    </>
  );
};
