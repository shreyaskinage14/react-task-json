import "../information";

const iHTML = () => {
  var web = document.getElementById("webtech");
  var boo = document.getElementById("bootstrapp");
  var jque = document.getElementById("jquery");
  var ihtml = document.getElementById("ihtml");
  var icss = document.getElementById("icss");

  if (ihtml.style.display === "none") {
    jque.style.display = "none";
    web.style.display = "none";
    boo.style.display = "none";
    ihtml.style.display = "block";
    icss.style.display = "none";
  } else {
    web.style.display = "block";
    ihtml.style.display = "none";
  }
};
export default iHTML;
