import "../information";

const jQue = () => {
  var web = document.getElementById("webtech");
  var boo = document.getElementById("bootstrapp");
  var jque = document.getElementById("jquery");
  var ihtml = document.getElementById("ihtml");
  var icss = document.getElementById("icss");

  if (jque.style.display === "none") {
    jque.style.display = "block";
    web.style.display = "none";
    boo.style.display = "none";
    ihtml.style.display = "none";
    icss.style.display = "none";
  } else {
    web.style.display = "block";
    jque.style.display = "none";
  }
};
export default jQue;
