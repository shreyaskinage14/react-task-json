import "../information";

const iCSS = () => {
  var web = document.getElementById("webtech");
  var boo = document.getElementById("bootstrapp");
  var jque = document.getElementById("jquery");
  var ihtml = document.getElementById("ihtml");
  var icss = document.getElementById("icss");

  if (icss.style.display === "none") {
    jque.style.display = "none";
    web.style.display = "none";
    boo.style.display = "none";
    ihtml.style.display = "none";
    icss.style.display = "block";
  } else {
    web.style.display = "block";
    icss.style.display = "none";
  }
};
export default iCSS;
