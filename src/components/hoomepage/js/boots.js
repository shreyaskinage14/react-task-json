import "../information";

const Boots = () => {
  var web = document.getElementById("webtech");
  var boo = document.getElementById("bootstrapp");
  var jque = document.getElementById("jquery");
  var ihtml = document.getElementById("ihtml");
  var icss = document.getElementById("icss");

  if (boo.style.display === "none") {
    jque.style.display = "none";
    web.style.display = "none";
    boo.style.display = "block";
    ihtml.style.display = "none";
    icss.style.display = "none";
  } else {
    web.style.display = "block";
    boo.style.display = "none";
  }
};
export default Boots;
