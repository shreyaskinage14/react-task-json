import React, { useState } from "react";
import { Form, Button, Card, Container } from "react-bootstrap";
import { Link } from "react-router-dom";

export const Signup = () => {
  var [username, setUsername] = useState("");
  var [password, setPassword] = useState("");
  var [user] = useState();
  user = {
    username: username,
    password: password,
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    var userPass = document.getElementById("pass").value;
    var userPassCon = document.getElementById("passcon").value;

    if (userPass === userPassCon) {
      localStorage.setItem("user", JSON.stringify(user));
    } else {
      window.alert("Passwords doesn't match");
    }
  };

  return (
    <>
      <Container
        className="d-flex align-items-center justify-content-center"
        style={{ minHeight: "100vh" }}
      >
        <div className="w-100" style={{ maxWidth: "400px" }}>
          <div
            class="jumbotron"
            style={{
              marginLeft: "65px",
              padding: "0px",
              backgroundColor: "white",
            }}
          >
            <h1>Web Technology</h1>
            <h5 className="d-flex justify-content-center mr-5">
              Welcome User!
            </h5>
          </div>

          <Card>
            <Card.Body>
              <h1 className="text-center mb-4">Register</h1>
              <Form onSubmit={handleSubmit}>
                <Form.Group id="username">
                  <Form.Label htmlFor="username">Username</Form.Label>
                  <Form.Control
                    id="user"
                    name="username"
                    autoComplete="off"
                    className="w-100"
                    value={username}
                    onChange={({ target }) => {
                      setUsername(target.value);
                    }}
                    type="username"
                    required
                  />
                </Form.Group>

                <Form.Group id="password">
                  <Form.Label htmlFor="password">Password</Form.Label>
                  <Form.Control
                    id="pass"
                    name="password"
                    value={password}
                    onChange={({ target }) => setPassword(target.value)}
                    type="password"
                    required
                  />
                </Form.Group>

                <Form.Group id="confirmpassword">
                  <Form.Label>Confirm Password</Form.Label>
                  <Form.Control id="passcon" type="password" required />
                </Form.Group>
                <Button
                  id="signup"
                  className="w-100 btn btn-dark"
                  type="submit"
                  onClick={handleSubmit}
                  disabled={!username || !password}
                >
                  <Link
                    to="/"
                    style={{ textDecoration: "none", color: "white" }}
                  >
                    Signup
                  </Link>
                </Button>
              </Form>
              <div className="w-100 text-center mt-2">
                Already have an account? <Link to="/">Login</Link>
              </div>
            </Card.Body>
          </Card>
        </div>
      </Container>
    </>
  );
};
