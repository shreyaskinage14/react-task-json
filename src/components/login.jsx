import React, { useState } from "react";
import { Form, Button, Card, Container } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";

export const Login = () => {
  const histo = useHistory();
  var [userna, setUserna] = useState("");
  var [passw, setPassw] = useState("");

  const handleSubmit = () => {
    var user = document.getElementById("user").value;
    var pass = document.getElementById("pass").value;
    var x = JSON.parse(localStorage.getItem("user"));

    if (user === x.username && pass === x.password) {
      histo.push("/hoome");
    } else {
      window.alert("Invalid Details!");
    }
  };

  return (
    <>
      <Container
        className="d-flex align-items-center justify-content-center"
        style={{ minHeight: "100vh" }}
      >
        <div className="w-100" style={{ maxWidth: "400px" }}>
          <div
            class="jumbotron"
            style={{
              marginLeft: "65px",
              padding: "0px",
              backgroundColor: "white",
            }}
          >
            <h1>Web Technology</h1>
            <h5 className="d-flex justify-content-center mr-5">
              Welcome Back!
            </h5>
          </div>

          <Card>
            <Card.Body>
              <h1 className="text-center mb-4">Login</h1>
              <Form>
                <Form.Group id="username">
                  <Form.Label>Username</Form.Label>
                  <Form.Control
                    id="user"
                    className="w-100"
                    autoComplete="off"
                    value={userna}
                    type="username"
                    onChange={({ target }) => {
                      setUserna(target.value);
                    }}
                    required
                  />
                </Form.Group>

                <Form.Group id="password">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    id="pass"
                    type="password"
                    value={passw}
                    onChange={({ target }) => {
                      setPassw(target.value);
                    }}
                    required
                  />
                </Form.Group>

                <Button
                  className="w-100 btn btn-dark"
                  id="login"
                  type="submit"
                  disabled={!userna || !passw}
                  onClick={handleSubmit}
                >
                  Login
                </Button>
              </Form>
              <div className="w-100 text-center mt-2">
                New here? <Link to="/signup">Signup</Link>
              </div>
            </Card.Body>
          </Card>
        </div>
      </Container>
    </>
  );
};
